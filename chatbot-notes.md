## Whois Bot

### What is does

Finds information from an URL.

Gets information from the domain registry and, possibly, complete/cross this with information from other sources.

### How it works

**Basic interaction**

1. Call the bot with whois + an URL:  
`jbotbot: whois jplusplus.org`
1. The bot will return basic contact information on the domain.


**More commands**

1. Send the bot a URL and get all the available informaion, including technical details such as DNS and name servers:
`jbotbot: whoisraw jplusplus.org`  
The bot will return the raw output of the domain whois. Since the output varies from registrar to registar it is dificult to parse the results into a structured format.

1. Get the list of actions you can prompt the bot with:  
`jbotbot: help`

1. See if the bot is up/running:  
`jbotbot: hi`


## Whois webservice

**API**

Query the API with `52.90.13.14:5000/query/string`. It will return parsed basic contact information and raw data.

For example `52.90.13.14:5000/domain/google.in` will return

    {
        "error": 0,
        "domain": {
            "data": {
                "person": [
                    "Christina Chiou"
                ],
                "organization": [
                    "Google Inc."
                ],
                "dateRegistered": [
                    "14-Feb-2005 20"
                ],
                "contact": [
                    "+1.6502530000",
                    "dns-admin@google.com"
                ]
            },
            "raw_data": "Access to .IN WHOIS information is provided to assist persons in determining the contents of a domain name registration record in the .IN registry database. The data in this record is provided by .IN Registry for informational purposes only, and .IN does not guarantee its accuracy.  This service is intended only for query-based access. You agree that you will use this data only for lawful purposes and that, under no circumstances will you use this data to(a) allow, enable, or otherwise support the transmission by e-mail, telephone, or facsimile of mass unsolicited, commercial advertising or solicitations to entities other than the data recipient's own existing customers; or (b) enable high volume, automated, electronic processes that send queries or data to the systems of Registry Operator, a Registrar, or Afilias except as reasonably necessary to register domain names or modify existing registrations. All rights reserved. .IN reserves the right to modify these terms at any time. By submitting this query, you agree to abide by this policy.\n\nDomain ID:D21089-AFIN\r\nDomain Name:GOOGLE.IN\r\nCreated On:14-Feb-2005 20:35:14 UTC\r\nLast Updated On:13-Jan-2016 10:22:51 UTC\r\nExpiration Date:14-Feb-2017 20:35:14 UTC\r\nSponsoring Registrar:MarkMonitor Inc. (R84-AFIN)\r\nStatus:CLIENT DELETE PROHIBITED\r\nReason:\r\nStatus:CLIENT TRANSFER PROHIBITED\r\nReason:\r\nStatus:CLIENT UPDATE PROHIBITED\r\nReason:\r\nRegistrant ID:mmr-108695\r\nRegistrant Name:Christina Chiou\r\nRegistrant Organization:Google Inc.\r\nRegistrant Street1:1600 Amphitheatre Parkway\r\nRegistrant Street2:\r\nRegistrant Street3:\r\nRegistrant City:Mountain View\r\nRegistrant State/Province:CA\r\nRegistrant Postal Code:94043\r\nRegistrant Country:US\r\nRegistrant Phone:+1.6502530000\r\nRegistrant Phone Ext.:\r\nRegistrant FAX:+1.6502530001\r\nRegistrant FAX Ext.:\r\nRegistrant Email:dns-admin@google.com\r\nAdmin ID:mmr-108695\r\nAdmin Name:Christina Chiou\r\nAdmin Organization:Google Inc.\r\nAdmin Street1:1600 Amphitheatre Parkway\r\nAdmin Street2:\r\nAdmin Street3:\r\nAdmin City:Mountain View\r\nAdmin State/Province:CA\r\nAdmin Postal Code:94043\r\nAdmin Country:US\r\nAdmin Phone:+1.6502530000\r\nAdmin Phone Ext.:\r\nAdmin FAX:+1.6502530001\r\nAdmin FAX Ext.:\r\nAdmin Email:dns-admin@google.com\r\nTech ID:mmr-108695\r\nTech Name:Christina Chiou\r\nTech Organization:Google Inc.\r\nTech Street1:1600 Amphitheatre Parkway\r\nTech Street2:\r\nTech Street3:\r\nTech City:Mountain View\r\nTech State/Province:CA\r\nTech Postal Code:94043\r\nTech Country:US\r\nTech Phone:+1.6502530000\r\nTech Phone Ext.:\r\nTech FAX:+1.6502530001\r\nTech FAX Ext.:\r\nTech Email:dns-admin@google.com\r\nName Server:NS1.GOOGLE.COM\r\nName Server:NS2.GOOGLE.COM\r\nName Server:NS3.GOOGLE.COM\r\nName Server:NS4.GOOGLE.COM\r\nName Server: \r\nName Server: \r\nName Server: \r\nName Server: \r\nName Server: \r\nName Server: \r\nName Server: \r\nName Server: \r\nName Server: \r\nDNSSEC:Unsigned\r\n\r\n\n",
            "message": "\n\nThe IP address of the server is 216.58.219.196. You might want to do another whois lookup on that, to see who owns the server (often a web hotel).",
            "ip": "216.58.219.196"
        }
    }


Check [here](http://52.90.13.14:5000) for the full API documentation.

### More

**How could we use the information provided by whois:**

* contact person details
** name
** telephone
** email
**find more based on this: twitter, avatar/profile pic, ...
* company details
** address -> link to address on map/directions
** telephone/fax -> _Call 000 000 000_
** email -> _Email xxx@x.y_
** find more based on these:  company logo, wikipedia page,...
* domain specific details available with the `raw` command
** status
** creation = registry date
** expiration date
** name servers
* show standard message for domains that don't provide information.  
Example `.es` domains: _Whois is only available on registration at https://sede.red.gob.es/sede/listado-dominios__


More information you could get from the webpage:
* Get the Google Analytics ID's in the page code
* Get any FB id
* Find the server location with traceroute + geolocation


## Problems

Domain information requests have a variety of outputs, depending on regional and local domain rules and registry companies.  
We started a file to document the whois outputs for some TLD domains and found some (for example .es and .gr) only make information available through a web interface with many restrictions. In some cases you have a limited requests, you must provide personal details, agree to terms of service and fill in captchas.

